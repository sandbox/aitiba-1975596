Provides a category for the user. If the admin has been selected a taxonomy term for this user,
when the user goes to create new Article, this article would be related to the taxonomy term
that admin has been selected for that user. If the admin has not been selected any taxonomy term,
the module look if there is a taxonomy term with the same username. If not, the new 
article would be related with the taxonomy term number 1.

The category is a taxonomy term for the first vocabulary created and named by default as "tags". 

As admin, on the user creation or edition, can select the user category.

How to use:

- Download and active the module.
- Select a category term for the user or create a category term with the same username.
- Create a new article.


Thanks for the help to @jonhattan, @pcambra and @rteijeiro
